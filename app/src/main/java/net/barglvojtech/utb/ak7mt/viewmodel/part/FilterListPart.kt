package net.barglvojtech.utb.ak7mt.viewmodel.part

import androidx.lifecycle.LiveData
import net.barglvojtech.utb.ak7mt.viewmodel.Filter

interface FilterListPart {
    val filters: LiveData<List<Filter>>
    val appliedFilter: LiveData<Filter>
    fun applyFilter(filter: Filter)
}

class SimpleFilterListPart(
    override val filters: LiveData<List<Filter>>,
    override val appliedFilter: LiveData<Filter>,
    private val filterApplier: (Filter) -> Unit,
) : FilterListPart {
    override fun applyFilter(filter: Filter) = filterApplier.invoke(filter)
}