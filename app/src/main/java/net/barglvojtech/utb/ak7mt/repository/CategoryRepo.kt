package net.barglvojtech.utb.ak7mt.repository

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.withContext
import net.barglvojtech.utb.ak7mt.domain.Category
import net.barglvojtech.utb.ak7mt.domain.toCategory
import net.barglvojtech.utb.ak7mt.domain.toPersistentCategory
import net.barglvojtech.utb.ak7mt.network.CocktailDbService
import net.barglvojtech.utb.ak7mt.persistance.CategoryDao
import net.barglvojtech.utb.ak7mt.util.android.buildLiveData
import net.barglvojtech.utb.ak7mt.util.concurrent.Concurrency

class CategoryRepo(
    private val service: CocktailDbService,
    private val categoryDao: CategoryDao,
) {
    private val categoriesChannel = Channel<List<Category>>()

    val categories = buildLiveData {
        emit(emptyList())

        suspend fun safeEmit(list: List<Category>) =
            withContext(Concurrency.Main + CoroutineName("category emitter")) { emit(list) }

        Concurrency.Main.launch(CoroutineName("loading categories")) {
            safeEmit(categoryDao.loadAll().map { it.toCategory() })
            while (!categoriesChannel.isClosedForReceive) kotlin.runCatching {
                safeEmit(categoriesChannel.receive())
            }
        }
    }

    suspend fun refreshCategories() =
        withContext(Concurrency.IO + CoroutineName("refreshing categories")) {
            val categories = service.listCategories()
            categoriesChannel.send(categories)
            categoryDao.insertAll(categories.map { it.toPersistentCategory() })
        }
}
