package net.barglvojtech.utb.ak7mt.viewmodel.part

import android.app.Application
import android.content.Intent
import net.barglvojtech.utb.ak7mt.domain.Cocktail
import net.barglvojtech.utb.ak7mt.ui.DetailActivity

interface ActivityPart {
    fun openDetails(cocktail: Cocktail)
}

class SimpleActivityPart(private val application: Application) : ActivityPart {
    override fun openDetails(cocktail: Cocktail) {
        application.startActivity(
            Intent(application.applicationContext, DetailActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                putExtra("cocktail_id", cocktail.id)
            }
        )
    }
}