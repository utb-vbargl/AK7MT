package net.barglvojtech.utb.ak7mt.network

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.HttpClientEngineFactory
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.DEFAULT
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.headers
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.contentType
import io.ktor.http.isSuccess
import io.ktor.serialization.kotlinx.json.json
import net.barglvojtech.utb.ak7mt.domain.Category
import net.barglvojtech.utb.ak7mt.domain.Cocktail
import net.barglvojtech.utb.ak7mt.domain.toCategory
import net.barglvojtech.utb.ak7mt.domain.toCocktail

class CocktailDbService(
    private val config: Configuration,
    httpClientEngine: HttpClientEngineFactory<*> = CIO
) {

    private val httpClient = HttpClient(httpClientEngine) {
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.BODY
        }
        install(ContentNegotiation) {
            json()
        }
    }

    suspend fun searchByName(name: String): List<Cocktail> =
        request<ResponseDrinks> { url("$it/search.php?s=$name") }
            .drinks.map { it.toCocktail() }

    suspend fun lookupById(id: String): Cocktail =
        request<ResponseDrinks> { url("$it/lookup.php?i=$id") }
            .drinks.first().toCocktail()

    suspend fun random(): Cocktail =
        request<ResponseDrinks> { url("$it/random.php") }
            .drinks.first().toCocktail()

    suspend fun listByCategory(category: String): List<Cocktail> =
        request<ResponsePartialDrinks> { url("$it/filter.php?c=$category") }
            .drinks.map { it.toCocktail() }

    suspend fun listRandom(): List<Cocktail> = buildList {
        repeat(10) { add(this@CocktailDbService.random()) }
    }

    suspend fun listCategories(): List<Category> =
        request<ResponseCategories> { url("$it/list.php?c=list") }
            .drinks.map { it.toCategory() }

    private suspend inline fun <reified T> request(
        method: HttpMethod = HttpMethod.Get,
        block: HttpRequestBuilder.(String) -> Unit
    ): T {
        val response = httpClient.request {
            this.method = method
            this.contentType(ContentType.Application.Json)
            block(config.publicUrl)
        }

        if (!response.status.isSuccess()) {
            error("${response.status}: ${response.bodyAsText()}")
        }

        return response.body()
    }

    companion object {
        const val PublicUrl = "https://www.thecocktaildb.com/api/json/v1/1/"
    }

    data class Configuration(
        val publicUrl: String = PublicUrl,
    )
}
