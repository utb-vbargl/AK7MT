package net.barglvojtech.utb.ak7mt.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import net.barglvojtech.utb.ak7mt.R
import net.barglvojtech.utb.ak7mt.databinding.ActivityDetailBinding
import net.barglvojtech.utb.ak7mt.util.android.dataBind
import net.barglvojtech.utb.ak7mt.viewmodel.SingleCocktailViewModel
import org.koin.android.ext.android.inject
import org.koin.android.scope.AndroidScopeComponent
import org.koin.androidx.scope.activityScope
import org.koin.core.parameter.parametersOf
import org.koin.core.scope.Scope

class DetailActivity : AppCompatActivity(), AndroidScopeComponent {
    override val scope: Scope by activityScope()

    private lateinit var cocktailId: String

    private val viewModel: SingleCocktailViewModel by inject { parametersOf(cocktailId) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cocktailId = intent.extras?.getString("cocktail_id")!!

        ActivityDetailBinding.inflate(layoutInflater)
            .dataBind({ lifecycle }) { cocktailPart = viewModel.cocktailPack }
            .also { setContentView(it.root) }

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            viewModel.cocktailPack.name.observe({ lifecycle }) { title = it }
        }

        viewModel.tap()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean = kotlin.runCatching {
        menu?.apply {
            menuInflater.inflate(R.menu.detail_menu, this)
            val favouriteButton = findItem(R.id.favourite)
            viewModel.isFavourite.observe(this@DetailActivity) { selected ->
                favouriteButton.setIcon(
                    if (selected) R.drawable.ic_favourite_filled
                    else R.drawable.ic_favourite_outlined
                )
            }
        }

    }.isSuccess

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.favourite -> {
                viewModel.viewModelScope.launch { viewModel.toggleFavourite() }
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }
}