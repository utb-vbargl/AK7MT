package net.barglvojtech.utb.ak7mt.persistance

import android.content.Context
import android.content.res.Resources
import java.io.File
import kotlin.io.path.Path
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.withContext
import net.barglvojtech.utb.ak7mt.R
import net.barglvojtech.utb.ak7mt.util.concurrent.Concurrency

class ImagesFilesystem(private val context: Context, resources: Resources) {
    private val path = resources.getString(R.string.images_path_key)

    private val filesDir: File
        get() = context.filesDir

    init {
        filesDir.resolve(path).mkdir()
    }

    suspend fun store(name: String, byteArray: ByteArray) =
        withContext(Concurrency.IO + CoroutineName("storing file $name")) {
            filesDir.with(name).outputStream().use { it.write(byteArray) }
        }


    suspend fun load(name: String): ByteArray =
        withContext(Concurrency.IO + CoroutineName("loading file $name")) {
            kotlin.runCatching { filesDir.with(name).readBytes() }
                .getOrDefault(ByteArray(0))
        }

    operator fun contains(name: String): Boolean =
        filesDir.with(name).exists()

    private fun File.with(name: String): File =
        filesDir.resolve(Path(this.path, name).toString())
}
