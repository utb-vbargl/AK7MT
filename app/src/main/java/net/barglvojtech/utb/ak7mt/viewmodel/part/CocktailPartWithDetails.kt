package net.barglvojtech.utb.ak7mt.viewmodel.part

import androidx.lifecycle.LiveData
import net.barglvojtech.utb.ak7mt.domain.Cocktail

interface CocktailPartWithDetails : CocktailPart {
    val isFavourite: LiveData<Boolean>
    fun toggleFavourite()
}