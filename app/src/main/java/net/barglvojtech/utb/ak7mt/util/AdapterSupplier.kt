package net.barglvojtech.utb.ak7mt.util

import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView

sealed interface AdapterSupplier {
    fun interface NoDependencyAdapterSupplier : AdapterSupplier {
        fun create(): RecyclerView.Adapter<*>
    }

    fun interface LiveDataAdapterSupplier : AdapterSupplier {
        fun create(lifecyclyOwner: LifecycleOwner): RecyclerView.Adapter<*>
    }
}