package net.barglvojtech.utb.ak7mt.persistance

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.RoomDatabase
import androidx.room.Transaction

@Database(entities = [Cocktail::class, Ingredient::class, Category::class], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun cocktailDao(): CocktailDao
    abstract fun categoryDao(): CategoryDao
}

@Dao
interface CocktailDao {
    @Transaction
    @Query("SELECT * FROM cocktail WHERE id = :id")
    suspend fun loadById(id: String): CocktailWithIngredients?

    @Transaction
    @Query("SELECT * FROM cocktail WHERE id IN (:ids)")
    suspend fun loadAllByIds(ids: List<String>): List<CocktailWithIngredients>

    @Transaction
    @Query("SELECT * FROM cocktail WHERE category = :category")
    fun loadAllByCategory(category: String): LiveData<List<CocktailWithIngredients>>

    suspend fun store(cocktailWithIngredients: CocktailWithIngredients) =
        cocktailWithIngredients.apply {
            store(cocktail)
            store(ingredients.map { it.copy(cocktailId = cocktail.id) })
        }

    @Insert(onConflict = REPLACE)
    fun store(cocktail: Cocktail)

    @Insert(onConflict = REPLACE)
    fun store(cocktail: List<Ingredient>)
}

@Dao
interface CategoryDao {
    @Query("SELECT * FROM category")
    suspend fun loadAll(): List<Category>

    @Insert(onConflict = IGNORE)
    suspend fun insertAll(categories: List<Category>)
}