package net.barglvojtech.utb.ak7mt.viewmodel

import net.barglvojtech.utb.ak7mt.domain.Category

sealed interface Filter {

    data class SearchCocktail(val query: String) : Filter
    data class CocktailsInCategory(val category: Category) : Filter {
        override fun toString(): String = category
    }

    object RecentCocktails : NamedFilter("Recent")
    object FavouriteCocktails : NamedFilter("Favourite")

    sealed class NamedFilter(val name: String) : Filter {
        override fun toString(): String = name
    }

    companion object {
        val StaticFilters get() = listOf(
            FavouriteCocktails,
            RecentCocktails,
        )

        fun forCategories(categories: List<Category>) =
            categories.map { CocktailsInCategory(it) }
    }
}