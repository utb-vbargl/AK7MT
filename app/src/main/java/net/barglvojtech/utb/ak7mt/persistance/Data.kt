package net.barglvojtech.utb.ak7mt.persistance

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

typealias ID = String

@Entity
data class Cocktail(
    @PrimaryKey val id: ID,
    val name: String,
    val category: String?,
    val imageUrl: String,
    val instructions: String?,
)

@Entity
data class Ingredient(
    @PrimaryKey val id: ID,
    val cocktailId: ID,
    val name: String,
    val volume: String,
    val unit: String
)

data class CocktailWithIngredients(
    @Embedded val cocktail: Cocktail,
    @Relation(
        parentColumn = "id",
        entityColumn = "cocktailId"
    )
    val ingredients: List<Ingredient>
)

@Entity
data class Category(
    @PrimaryKey
    val name: String
)