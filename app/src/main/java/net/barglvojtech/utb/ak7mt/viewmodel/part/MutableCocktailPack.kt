package net.barglvojtech.utb.ak7mt.viewmodel.part

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData

interface MutableCocktailPack {
    val name: MutableLiveData<String>
    val category: MutableLiveData<String>
    val imageUrl: MutableLiveData<String>
    val image: MutableLiveData<Bitmap>
}

class SimpleMutableCocktailPack : MutableCocktailPack {
    override val name: MutableLiveData<String> = MutableLiveData()
    override val category: MutableLiveData<String> = MutableLiveData()
    override val imageUrl: MutableLiveData<String> = MutableLiveData()
    override val image: MutableLiveData<Bitmap> = MutableLiveData()
}