package net.barglvojtech.utb.ak7mt.viewmodel.part

fun interface SearchPart {
    fun search(value: String)
}