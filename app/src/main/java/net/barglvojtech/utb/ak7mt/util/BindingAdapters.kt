package net.barglvojtech.utb.ak7mt.util

import android.content.Context.INPUT_METHOD_SERVICE
import android.graphics.BitmapFactory
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.findViewTreeLifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import androidx.recyclerview.widget.RecyclerView
import net.barglvojtech.utb.ak7mt.R
import net.barglvojtech.utb.ak7mt.viewmodel.part.SearchPart

object BindingAdapters {

    @BindingAdapter(value = ["setOnEditorActionListener"])
    @JvmStatic
    fun setOnEditorActionListener(view: EditText, action: SearchPart) {
        view.setOnEditorActionListener(TextView.OnEditorActionListener { _, _, _ ->

            action.search(view.text.toString())
            (view.context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
                .hideSoftInputFromWindow(view.windowToken, 0)

            return@OnEditorActionListener true
        })
    }

    @BindingAdapter(value = ["bitmap"])
    @JvmStatic
    fun setBitmapImage(view: ImageView, bitmap: ByteArray?) {
        if (bitmap?.isEmpty() == false) {
            view.setImageBitmap(BitmapFactory.decodeStream(bitmap.inputStream()))
        } else {
            view.setImageResource(R.drawable.placeholder)
        }
    }

    @BindingAdapter(value = ["layout_manager"])
    @JvmStatic
    fun setLayoutManager(view: RecyclerView, layoutManager: LayoutManager) {
        view.layoutManager = when (layoutManager) {
            LayoutManager.LinearHorizontal ->
                LinearLayoutManager(view.context).apply { orientation = HORIZONTAL }
            LayoutManager.LinearVertical ->
                LinearLayoutManager(view.context).apply { orientation = VERTICAL }
        }
    }

    enum class LayoutManager {
        LinearHorizontal,
        LinearVertical,
    }

    @BindingAdapter(value = ["provide_adapter"])
    @JvmStatic
    fun provideAdapter(view: RecyclerView, adapterSupplier: AdapterSupplier) {
        view.adapter = when (adapterSupplier) {
            is AdapterSupplier.NoDependencyAdapterSupplier -> adapterSupplier.create()
            is AdapterSupplier.LiveDataAdapterSupplier -> adapterSupplier.create(
                view.findViewTreeLifecycleOwner()
                    ?: throw IllegalStateException("no LifecycleOwner found")
            )
        }
    }
}