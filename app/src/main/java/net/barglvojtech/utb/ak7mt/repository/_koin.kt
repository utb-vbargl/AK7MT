package net.barglvojtech.utb.ak7mt.repository

import org.koin.core.module.dsl.new
import org.koin.core.qualifier.named
import org.koin.dsl.module

object RepositoryKoin {
    val module = module {
        single { new(::CategoryRepo) }
        single { new(::CocktailRepo) }
        single { new(::RecentCocktailsRepo) }
        single { new(::FavouriteCocktailsRepo) }
        single { new(::RandomCocktailsRepo) }
    }
}