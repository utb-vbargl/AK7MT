package net.barglvojtech.utb.ak7mt.viewmodel.part

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

interface BackgroundJobPack {
    fun launch(block: suspend CoroutineScope.() -> Unit)
}

open class SimpleBackgroundJobPack(private val scope: CoroutineScope) : BackgroundJobPack {
    override fun launch(block: suspend CoroutineScope.() -> Unit) {
        scope.launch(block = block)
    }
}