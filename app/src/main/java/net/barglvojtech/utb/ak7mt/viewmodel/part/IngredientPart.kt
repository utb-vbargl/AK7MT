package net.barglvojtech.utb.ak7mt.viewmodel.part

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import net.barglvojtech.utb.ak7mt.domain.Ingredient
import net.barglvojtech.utb.ak7mt.util.android.deriveLiveData

interface IngredientPart {
    val ingredient: LiveData<Ingredient>

    val checked: MutableLiveData<Boolean>
    val name: LiveData<String>
    val volume: LiveData<String>
    val unit: LiveData<String>
}

class SimpleIngredientPart(
    override val ingredient: LiveData<Ingredient>
) : IngredientPart {

    override val checked = MutableLiveData<Boolean>()
    override val name = deriveLiveData(ingredient) { it.name }
    override val volume = deriveLiveData(ingredient) { it.volume }
    override val unit = deriveLiveData(ingredient) { it.unit }
}