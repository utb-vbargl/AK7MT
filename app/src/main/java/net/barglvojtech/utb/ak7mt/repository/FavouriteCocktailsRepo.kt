package net.barglvojtech.utb.ak7mt.repository

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import net.barglvojtech.utb.ak7mt.persistance.FavouriteCocktailsPreferences
import net.barglvojtech.utb.ak7mt.persistance.ID
import net.barglvojtech.utb.ak7mt.util.android.buildLiveData
import net.barglvojtech.utb.ak7mt.util.concurrent.Concurrency

class FavouriteCocktailsRepo(
    private val favouriteCocktailsPreferences: FavouriteCocktailsPreferences
) {
    private val syncChannel = Channel<List<ID>>()

    val data = buildLiveData {
        emit(favouriteCocktailsPreferences.load())

        Concurrency.Main.launch {
            while (true) {
                emit(syncChannel.receive())
            }
        }
    }

    suspend fun toggle(id: String) = coroutineScope {
        val newList = buildList {
            val (with, without) = (data.value ?: emptyList()).partition { it == id }
            if (with.isEmpty()) {
                add(id)
            }
            addAll(without)
        }

        syncChannel.send(newList)
        favouriteCocktailsPreferences.update(newList)
    }
}