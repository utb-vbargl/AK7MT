package net.barglvojtech.utb.ak7mt.util.concurrent

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

val exceptionHandler = CoroutineExceptionHandler { c, throwable ->
    Log.e("ERROR", "exception occurred in: ${c[CoroutineName]?.name}", throwable)
}

class Concurrency(context: CoroutineContext) : CoroutineContext by context {
    private val scope: CoroutineScope
        get() = CoroutineScope(this)

    fun launch(
        context: CoroutineContext = EmptyCoroutineContext,
        start: CoroutineStart = CoroutineStart.DEFAULT,
        block: suspend CoroutineScope.() -> Unit
    ) = scope.launch(context, start, block)

    companion object {
        val IO = Concurrency(Dispatchers.IO + exceptionHandler)
        val Main = Concurrency(Dispatchers.Main + SupervisorJob() + exceptionHandler)

        context(ViewModel)
        val ViewModel get() = Concurrency(viewModelScope.coroutineContext + exceptionHandler)
    }
}

