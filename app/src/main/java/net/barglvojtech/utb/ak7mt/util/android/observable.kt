package net.barglvojtech.utb.ak7mt.util.android

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun Lifecycle.observeOnce(event: Lifecycle.Event, block: () -> Unit) = addObserver(
    when (event) {
        Lifecycle.Event.ON_CREATE -> JustOnceLifecycleObserver(onCreate = block)
        Lifecycle.Event.ON_START -> JustOnceLifecycleObserver(onStart = block)
        Lifecycle.Event.ON_RESUME -> JustOnceLifecycleObserver(onResume = block)
        Lifecycle.Event.ON_PAUSE -> JustOnceLifecycleObserver(onPause = block)
        Lifecycle.Event.ON_STOP -> JustOnceLifecycleObserver(onStop = block)
        Lifecycle.Event.ON_DESTROY -> JustOnceLifecycleObserver(onDestroy = block)
        Lifecycle.Event.ON_ANY -> JustOnceLifecycleObserver(
            onCreate = block,
            onStart = block,
            onResume = block,
            onPause = block,
            onStop = block,
            onDestroy = block
        )
    }
)

private typealias Action = () -> Unit

private class JustOnceLifecycleObserver(
    private val onCreate: Action? = null,
    private val onStart: Action? = null,
    private val onResume: Action? = null,
    private val onPause: Action? = null,
    private val onStop: Action? = null,
    private val onDestroy: Action? = null,
) : DefaultLifecycleObserver {

    override fun onCreate(owner: LifecycleOwner) = cleanAndInvoke(owner, onCreate)
    override fun onStart(owner: LifecycleOwner) = cleanAndInvoke(owner, onStart)
    override fun onResume(owner: LifecycleOwner) = cleanAndInvoke(owner, onResume)
    override fun onPause(owner: LifecycleOwner) = cleanAndInvoke(owner, onPause)
    override fun onStop(owner: LifecycleOwner) = cleanAndInvoke(owner, onStop)
    override fun onDestroy(owner: LifecycleOwner) = cleanAndInvoke(owner, onDestroy)

    private fun cleanAndInvoke(owner: LifecycleOwner, action: Action?) {
        if (action != null) {
            owner.lifecycle.removeObserver(this@JustOnceLifecycleObserver)
            action()
        }
    }
}