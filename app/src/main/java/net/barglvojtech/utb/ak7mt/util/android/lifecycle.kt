package net.barglvojtech.utb.ak7mt.util.android

import androidx.lifecycle.Lifecycle.State as LifecycleState
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry



class ControllableLifecycle : LifecycleOwner {

    private val lifecycleRegistry = LifecycleRegistry(this)
    private var paused: Boolean = false

    init {
        lifecycleRegistry.currentState = LifecycleState.INITIALIZED
    }

    fun onCreated() {
        lifecycleRegistry.currentState = LifecycleState.CREATED
    }

    fun onStartedResumed() {
        lifecycleRegistry.currentState = when {
            paused -> LifecycleState.RESUMED
            else -> LifecycleState.STARTED
        }
        paused = false
    }

    fun onPaused() {
        paused = true
        lifecycleRegistry.currentState = LifecycleState.CREATED
    }

    fun onDestroy() {
        lifecycleRegistry.currentState = LifecycleState.DESTROYED
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }
}


