package net.barglvojtech.utb.ak7mt.repository

import kotlinx.coroutines.channels.Channel
import net.barglvojtech.utb.ak7mt.domain.ID
import net.barglvojtech.utb.ak7mt.network.CocktailDbService
import net.barglvojtech.utb.ak7mt.persistance.RandomCocktailsPreferences
import net.barglvojtech.utb.ak7mt.util.android.buildLiveData
import net.barglvojtech.utb.ak7mt.util.concurrent.Concurrency

class RandomCocktailsRepo(
    private val cocktailService: CocktailDbService,
    private val randomCocktailsPreferences: RandomCocktailsPreferences,
    private val cocktailRepo: CocktailRepo,
) {
    private val syncChannel = Channel<List<ID>>()

    val data = buildLiveData {
        emit(randomCocktailsPreferences.load())

        Concurrency.Main.launch {
            while (true) {
                emit(syncChannel.receive())
            }
        }
    }

    suspend fun refresh() {
        val ids = cocktailService.listRandom()
            .onEach { cocktailRepo.blend(it) }
            .map { it.id }

        syncChannel.send(ids)
        randomCocktailsPreferences.update(ids)
    }
}