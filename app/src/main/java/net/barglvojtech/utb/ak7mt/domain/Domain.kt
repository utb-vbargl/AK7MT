package net.barglvojtech.utb.ak7mt.domain

typealias ID = String
typealias Category = String

data class Cocktail(
    val id: ID,
    val name: String,
    val category: Category?,
    val imageUrl: String,
    val image: ByteArray,
    val instructions: String?,
    val ingredients: List<Ingredient>
)

data class Ingredient(
    val id: ID,
    val name: String,
    val volume: String,
    val unit: String
)