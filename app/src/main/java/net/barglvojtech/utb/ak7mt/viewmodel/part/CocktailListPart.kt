package net.barglvojtech.utb.ak7mt.viewmodel.part

import androidx.lifecycle.LiveData
import net.barglvojtech.utb.ak7mt.domain.Cocktail

interface CocktailListPart {
    val cocktails: LiveData<List<String>>
    fun getCocktail(id: String): CocktailPart
}

class SimpleCocktailListPart(
    override val cocktails: LiveData<List<String>>,
    private val cocktailSupplier: (String) -> LiveData<Cocktail>,
) : CocktailListPart {
    override fun getCocktail(id: String): CocktailPart =
        SimpleCocktailPart(cocktailSupplier(id))
}