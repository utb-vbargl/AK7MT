package net.barglvojtech.utb.ak7mt.network

import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngineFactory
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.logging.DEFAULT
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.client.statement.bodyAsChannel
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpMethod
import io.ktor.http.isSuccess
import io.ktor.util.toByteArray
import io.ktor.utils.io.ByteReadChannel

class SimpleHttpService(httpClientEngine: HttpClientEngineFactory<*> = CIO) {

    private val httpClient = HttpClient(httpClientEngine) {
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.BODY
        }
    }

    suspend fun downloadImage(url: String): ByteArray =
        request { url(url) }.toByteArray()

    private suspend inline fun request(
        method: HttpMethod = HttpMethod.Get,
        block: HttpRequestBuilder.() -> Unit
    ): ByteReadChannel {
        val response = httpClient.request {
            this.method = method
            block()
        }

        if (!response.status.isSuccess()) {
            error("${response.status}: ${response.bodyAsText()}")
        }

        return response.bodyAsChannel()
    }

}

