package net.barglvojtech.utb.ak7mt.viewmodel

import androidx.lifecycle.ViewModel
import net.barglvojtech.utb.ak7mt.repository.CocktailRepo
import net.barglvojtech.utb.ak7mt.repository.FavouriteCocktailsRepo
import net.barglvojtech.utb.ak7mt.repository.RecentCocktailsRepo
import net.barglvojtech.utb.ak7mt.util.android.buildLiveData
import net.barglvojtech.utb.ak7mt.util.concurrent.Concurrency
import net.barglvojtech.utb.ak7mt.viewmodel.part.SimpleCocktailPart

class SingleCocktailViewModel(
    private val cocktailId: String,
    private val cocktailsRepo: CocktailRepo,
    private val recentCocktailsRepo: RecentCocktailsRepo,
    private val favouriteCocktailsRepo: FavouriteCocktailsRepo,
) : ViewModel() {
    val cocktailPack by lazy { SimpleCocktailPart(cocktailsRepo.load(cocktailId)) }

    val isFavourite = buildLiveData {
        emit(false)

        observe(favouriteCocktailsRepo.data) { favourites ->
            emit(cocktailId in favourites)
        }
    }

    fun tap() = Concurrency.ViewModel.launch {
        recentCocktailsRepo.add(cocktailId)
    }

    fun toggleFavourite() = Concurrency.ViewModel.launch {
        favouriteCocktailsRepo.toggle(cocktailId)
    }
}