package net.barglvojtech.utb.ak7mt

import net.barglvojtech.utb.ak7mt.network.CocktailDbService
import net.barglvojtech.utb.ak7mt.network.NetworkKoin
import net.barglvojtech.utb.ak7mt.persistance.PersistanceKoin
import net.barglvojtech.utb.ak7mt.repository.RepositoryKoin
import net.barglvojtech.utb.ak7mt.viewmodel.ViewModelKoin
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinApplication
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module

class Application : android.app.Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@Application)
            modules(
                configuration,
                PersistanceKoin.module,
                NetworkKoin.module,
                RepositoryKoin.module,
                ViewModelKoin.module
            )
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        stopKoin()
    }

    private val configuration = module {
        single { this }
        single { CocktailDbService.Configuration() }
        single { getSharedPreferences(getString(R.string.preference_key), MODE_PRIVATE) }
        single { applicationContext }
        single { resources }
    }
}