@file:OptIn(ExperimentalCoroutinesApi::class)

package net.barglvojtech.utb.ak7mt.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.selects.whileSelect
import kotlinx.coroutines.withContext
import net.barglvojtech.utb.ak7mt.domain.ID
import net.barglvojtech.utb.ak7mt.network.CocktailDbService
import net.barglvojtech.utb.ak7mt.repository.CategoryRepo
import net.barglvojtech.utb.ak7mt.repository.CocktailRepo
import net.barglvojtech.utb.ak7mt.repository.FavouriteCocktailsRepo
import net.barglvojtech.utb.ak7mt.repository.RandomCocktailsRepo
import net.barglvojtech.utb.ak7mt.repository.RecentCocktailsRepo
import net.barglvojtech.utb.ak7mt.util.android.LiveDataBuilder
import net.barglvojtech.utb.ak7mt.util.android.buildLiveData
import net.barglvojtech.utb.ak7mt.util.android.observeOnly
import net.barglvojtech.utb.ak7mt.util.concurrent.Concurrency
import net.barglvojtech.utb.ak7mt.viewmodel.part.SearchPart
import net.barglvojtech.utb.ak7mt.viewmodel.part.SimpleActivityPart
import net.barglvojtech.utb.ak7mt.viewmodel.part.SimpleCocktailListPart
import net.barglvojtech.utb.ak7mt.viewmodel.part.SimpleFilterListPart

class CocktailsAndFiltersViewModel(
    application: Application,
    private val cocktailService: CocktailDbService,
    private val cocktailsRepo: CocktailRepo,
    private val categoryRepo: CategoryRepo,
    private val favouriteCocktailsRepo: FavouriteCocktailsRepo,
    private val recentCocktailsRepo: RecentCocktailsRepo,
    private val randomCocktailsRepo: RandomCocktailsRepo,
) : ViewModel() {

    private val cocktails = object {
        private val syncChannel = Channel<CocktailsRequest>()

        val data = buildLiveData {
            emit(emptyList())

            Concurrency.Main.launch(CoroutineName("cocktail loader")) {
                while (true) when (val request = syncChannel.receive()) {
                    is CocktailsRequest.SetList -> emit(request.list)
                    is CocktailsRequest.SetLiveData -> observeOnly(request.liveData)
                }
            }
        }

        suspend fun emit(list: List<ID>) {
            syncChannel.send(CocktailsRequest.SetList(list))
        }

        suspend fun observe(liveData: LiveData<List<ID>>) {
            syncChannel.send(CocktailsRequest.SetLiveData(liveData))
        }
    }

    private val filters = object {
        val data = buildLiveData {
            emit(Filter.StaticFilters)

            observeOnly(categoryRepo.categories) {
                emit(Filter.StaticFilters + Filter.forCategories(it))
            }
        }

        val applied = MutableLiveData<Filter>()

        fun apply(filter: Filter) = Concurrency.ViewModel.launch {
            applied.value = filter
            when (filter) {
                is Filter.RecentCocktails ->
                    cocktails.observe(recentCocktailsRepo.data)
                is Filter.FavouriteCocktails ->
                    cocktails.observe(favouriteCocktailsRepo.data)
                is Filter.CocktailsInCategory -> {
                    cocktails.emit(emptyList())

                    val ids = cocktailService.listByCategory(filter.category)
                        .onEach { cocktailsRepo.blend(it) }
                        .map { it.id }

                    cocktails.emit(ids)
                }
                is Filter.SearchCocktail -> {
                    cocktails.emit(emptyList())

                    val ids = cocktailService.searchByName(filter.query)
                        .onEach { cocktailsRepo.blend(it) }
                        .map { it.id }

                    cocktails.emit(ids)
                }
            }
        }
    }

    val activityPart = SimpleActivityPart(application)
    val searchPart = SearchPart { value -> filters.apply(Filter.SearchCocktail(value)) }
    val filterListPart = SimpleFilterListPart(filters.data, filters.applied, filters::apply)
    val cocktailsListPart = SimpleCocktailListPart(cocktails.data, cocktailsRepo::load)
    val randomCocktailListPart =
        SimpleCocktailListPart(randomCocktailsRepo.data, cocktailsRepo::load)

    init {
        filters.apply(Filter.FavouriteCocktails)
        Concurrency.Main.launch {
            categoryRepo.refreshCategories()
            randomCocktailsRepo.refresh()
        }
    }

    private sealed interface CocktailsRequest {
        data class SetLiveData(val liveData: LiveData<List<String>>) : CocktailsRequest
        data class SetList(val list: List<String>) : CocktailsRequest
    }
}

