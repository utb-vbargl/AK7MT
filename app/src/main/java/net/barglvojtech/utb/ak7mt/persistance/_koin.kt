package net.barglvojtech.utb.ak7mt.persistance

import android.app.Application
import androidx.room.Room
import kotlin.reflect.jvm.internal.impl.descriptors.impl.ModuleDependencies
import org.koin.core.module.Module
import org.koin.core.module.dsl.new
import org.koin.core.qualifier.named
import org.koin.dsl.module


object PersistanceKoin {
    val module = module {
        fun buildDatabase(app: Application): Database = Room.databaseBuilder(
            app.applicationContext, Database::class.java, "database"
        ).build()

        single { new(::buildDatabase) }
        single { new(Database::cocktailDao) }
        single { new(Database::categoryDao) }

        single { new(::ImagesFilesystem) }

        single { new(::RandomCocktailsPreferences) }
        single { new(::FavouriteCocktailsPreferences) }
        single { new(::RecentCocktailsPreferences) }
    }
}