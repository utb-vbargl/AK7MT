package net.barglvojtech.utb.ak7mt.persistance

import android.content.SharedPreferences
import android.content.res.Resources
import androidx.core.content.edit
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.barglvojtech.utb.ak7mt.R


interface Preferences<T> {
    fun load(): T
    fun update(value: T)
}

private class ListPreferences(
    private val preferences: SharedPreferences,
    private val key: String,
) : Preferences<List<ID>> {

    override fun load(): List<ID> =
        preferences.getString(key, emptyList)!!.let(::decode).modified()

    override fun update(value: List<ID>) = preferences.edit(commit = true) {
        when {
            value.isEmpty() -> remove(key)
            else -> putString(key, encode(value.modified()))
        }
    }

    companion object {
        private const val preferredSize = 10
        private const val emptyList = "[]"
        fun encode(value: List<ID>): String = Json.encodeToString(value)
        fun decode(value: String): List<ID> = Json.decodeFromString(value)


        fun List<ID>.modified() = distinct().take(preferredSize)
    }
}

class RandomCocktailsPreferences(
    preferences: SharedPreferences,
    resources: Resources
) : Preferences<List<ID>> by ListPreferences(
    preferences, resources.getString(R.string.random_cocktails_key)
)

class RecentCocktailsPreferences(
    preferences: SharedPreferences,
    resources: Resources
) : Preferences<List<ID>> by ListPreferences(
    preferences, resources.getString(R.string.recent_cocktails_key)
)

class FavouriteCocktailsPreferences(
    preferences: SharedPreferences,
    resources: Resources
) : Preferences<List<ID>> by ListPreferences(
    preferences, resources.getString(R.string.favourite_cocktails_key)
)