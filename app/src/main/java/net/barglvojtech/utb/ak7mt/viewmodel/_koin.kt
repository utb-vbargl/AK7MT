package net.barglvojtech.utb.ak7mt.viewmodel

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.dsl.new
import org.koin.dsl.module

object ViewModelKoin {
    val module = module {
        viewModel { new(::CocktailsAndFiltersViewModel) }
        viewModel { new(::SingleCocktailViewModel) }
    }
}