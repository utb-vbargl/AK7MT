package net.barglvojtech.utb.ak7mt.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.barglvojtech.utb.ak7mt.domain.Cocktail
import net.barglvojtech.utb.ak7mt.domain.ID
import net.barglvojtech.utb.ak7mt.domain.blendWith
import net.barglvojtech.utb.ak7mt.domain.toCocktail
import net.barglvojtech.utb.ak7mt.domain.toPersistentCocktail
import net.barglvojtech.utb.ak7mt.network.CocktailDbService
import net.barglvojtech.utb.ak7mt.network.SimpleHttpService
import net.barglvojtech.utb.ak7mt.persistance.CocktailDao
import net.barglvojtech.utb.ak7mt.persistance.ImagesFilesystem
import net.barglvojtech.utb.ak7mt.util.concurrent.Concurrency

/** CocktailRepo handles cocktails in multiple ways:
 * - load from db
 * - refresh from internet
 * - provide to ui
 */
class CocktailRepo(
    private val cocktailDao: CocktailDao,
    private val cocktailService: CocktailDbService,
    private val httpService: SimpleHttpService,
    private val imagesFilesystem: ImagesFilesystem,
) {
    private val cache = mutableMapOf<ID, MutableLiveData<Cocktail>>()

    fun load(id: String): LiveData<Cocktail> {
        val data = cache.getOrPut(id) { MutableLiveData(Cocktail(id)) }
        Concurrency.IO.launch { loadAsync(id) }
        return data
    }

    /** Blend puts cocktail into cache if it is not and fills missing fields if necessary. */
    fun blend(cocktail: Cocktail): LiveData<Cocktail> {
        val data = cache.compute(cocktail.id) { _, old ->
            if (old != null) {
                Concurrency.Main.launch { old.value = old.value!!.blendWith(cocktail) }
                return@compute old
            } else {
                return@compute MutableLiveData(cocktail)
            }
        }
        Concurrency.IO.launch { fetchAndStoreAsync(cocktail.id, forceStore = true) }
        return data!!
    }

    private suspend fun loadAsync(id: String) =
        updateCache("loading", id) {
            launch {
                val loaded = cocktailDao.loadById(latest.id)?.toCocktail()
                update { it.blendWith(loaded) }
            }
            launch {
                val loaded = latest.copy(image = imagesFilesystem.load(latest.id))
                update { it.blendWith(loaded) }
            }
        }

    private suspend fun fetchAndStoreAsync(
        id: String,
        forceFetch: Boolean = false,
        forceStore: Boolean = false,
    ) = updateCache("fetching", id) {
        if (forceFetch || latest.isMissingDetails) launch {
            val loaded = cocktailService.lookupById(latest.id)
            update { it.blendWith(loaded) }
            cocktailDao.store(latest.toPersistentCocktail())
        } else if (forceStore) launch {
            cocktailDao.store(latest.toPersistentCocktail())
        }

        if (forceFetch || latest.isMissingImage) launch {
            val loaded = latest.copy(image = httpService.downloadImage(latest.imageUrl))
            update { it.blendWith(loaded) }
            imagesFilesystem.store(latest.id, latest.image)
        } else if (forceStore) launch {
            imagesFilesystem.store(latest.id, latest.image)
        }
    }

    private suspend fun updateCache(
        method: String, id: String, block: suspend DataManipulationScope<Cocktail>.() -> Unit
    ) = withContext(Concurrency.IO + CoroutineName("$method $id")) {
        val data = cache[id] ?: error("need to blend $id first")
        block(DataManipulationScope(Concurrency.IO, data))
    }

    companion object {
        private fun Cocktail(id: String) =
            Cocktail(id, "loading...", null, "", ByteArray(0), null, emptyList())
    }

}

private val Cocktail.isMissingDetails get() = instructions == null
private val Cocktail.isMissingImage get() = image.isEmpty()
