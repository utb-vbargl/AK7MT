package net.barglvojtech.utb.ak7mt.util.android

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import kotlinx.coroutines.DisposableHandle

interface LiveDataBuilder<T> {
    val latestValue: T?
    fun emit(value: T)
}

interface ObservableLiveDataBuilder<T> : LiveDataBuilder<T> {
    fun <U> observe(source: LiveData<U>, block: LiveDataBuilder<T>.(U) -> Unit): DisposableHandle
    fun <U> observeOnly(source: LiveData<U>, block: LiveDataBuilder<T>.(U) -> Unit): DisposableHandle
}

fun <T> ObservableLiveDataBuilder<T>.observe(source: LiveData<T>): DisposableHandle =
    observe(source) { emit(it) }

fun <T> ObservableLiveDataBuilder<T>.observeOnly(source: LiveData<T>): DisposableHandle =
    observeOnly(source) { emit(it) }

fun <T> buildLiveData(
    block: ObservableLiveDataBuilder<T>.() -> Unit,
): LiveData<T> = MediatorLiveData<T>().also { block(SimpleLiveDataBuilder(it)) }

fun <T, R> deriveLiveData(
    data: LiveData<T>,
    block: (T) -> R
) = buildLiveData { observe(data) { emit(block(it)) } }

private class SimpleLiveDataBuilder<T>(
    private val data: MediatorLiveData<T>
) : ObservableLiveDataBuilder<T> {

    private var watchedSources = emptyList<DisposableSource>()

    override val latestValue: T?
        get() = data.value

    override fun emit(value: T) {
        disposeAll()
        doEmit(value)
    }

    override fun <U> observe(source: LiveData<U>, block: LiveDataBuilder<T>.(U) -> Unit): DisposableSource {
        val innerBuilder = object : LiveDataBuilder<T> {
            override val latestValue: T? get() = this@SimpleLiveDataBuilder.latestValue
            override fun emit(value: T) = this@SimpleLiveDataBuilder.doEmit(value)
        }

        data.addSource(source) { innerBuilder.block(it) }

        val disposableSource = DisposableSource(source)
        watchedSources = watchedSources + disposableSource
        return disposableSource
    }

    override fun <U> observeOnly(source: LiveData<U>, block: LiveDataBuilder<T>.(U) -> Unit): DisposableSource {
        disposeAll()
        return observe(source, block)
    }

    private fun disposeAll() {
        watchedSources.forEach { it.dispose() }
        watchedSources = emptyList()
    }

    @SuppressLint("NullSafeMutableLiveData")
    private fun doEmit(value: T) {
        data.value = value
    }

    inner class DisposableSource(val source: LiveData<*>) : DisposableHandle {
        override fun dispose() {
            data.removeSource(source)
        }
    }
}