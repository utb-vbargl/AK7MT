package net.barglvojtech.utb.ak7mt.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlin.coroutines.CoroutineContext
import kotlin.properties.ReadOnlyProperty
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext
import net.barglvojtech.utb.ak7mt.util.concurrent.Concurrency

class DataManipulationScope<T>(
    context: CoroutineContext,
    private val mutableData: MutableLiveData<T>
) : CoroutineScope by CoroutineScope(context) {
    val data: LiveData<T>
        get() = mutableData

    val latest by ReadOnlyProperty { _, _ -> mutableData.value!! }

    suspend fun update(block: suspend (T) -> T) =
        withContext(Concurrency.Main + CoroutineName("data emitter")) {
            mutableData.value = block(mutableData.value!!)
        }
}