package net.barglvojtech.utb.ak7mt.util.android

import androidx.activity.ComponentActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner

fun <B : ViewDataBinding> ComponentActivity.dataBinding(
    id: Int,
    block: B.() -> Unit
): B = DataBindingUtil.setContentView<B>(this, id)
        .also { it.lifecycleOwner = this }
        .apply(block)

fun <T : ViewDataBinding> T.dataBind(lifecycleOwner: LifecycleOwner, block: T.() -> Unit): T {
    this.lifecycleOwner = lifecycleOwner
    block()
    return this
}
