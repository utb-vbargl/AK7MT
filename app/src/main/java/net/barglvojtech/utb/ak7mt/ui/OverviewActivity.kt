package net.barglvojtech.utb.ak7mt.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle.Event.ON_DESTROY
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import androidx.recyclerview.widget.ListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import net.barglvojtech.utb.ak7mt.R
import net.barglvojtech.utb.ak7mt.databinding.ActivityOverviewBinding
import net.barglvojtech.utb.ak7mt.databinding.FragmentCocktailCardBinding
import net.barglvojtech.utb.ak7mt.databinding.FragmentCocktailQuickFiltersBinding
import net.barglvojtech.utb.ak7mt.databinding.FragmentPopularCocktailsBinding
import net.barglvojtech.utb.ak7mt.databinding.FragmentSearchCocktailBinding
import net.barglvojtech.utb.ak7mt.util.android.ControllableLifecycle
import net.barglvojtech.utb.ak7mt.util.android.observeOnce
import net.barglvojtech.utb.ak7mt.viewmodel.CocktailsAndFiltersViewModel
import net.barglvojtech.utb.ak7mt.viewmodel.part.ActivityPart
import net.barglvojtech.utb.ak7mt.viewmodel.part.CocktailListPart
import net.barglvojtech.utb.ak7mt.viewmodel.part.FilterListPart
import net.barglvojtech.utb.ak7mt.viewmodel.part.SearchPart
import org.koin.android.ext.android.inject
import org.koin.android.scope.AndroidScopeComponent
import org.koin.androidx.scope.activityScope
import org.koin.core.scope.Scope

class OverviewActivity : AppCompatActivity(), AndroidScopeComponent {
    override val scope: Scope by activityScope()

    private val viewModel: CocktailsAndFiltersViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()

        val binding = ActivityOverviewBinding.inflate(layoutInflater)
            .also { setContentView(it.root) }

        binding.overview.apply {
            layoutManager = LinearLayoutManager(context).apply { orientation = VERTICAL }
            adapter = OverviewAdapter(
                this@OverviewActivity, OverviewAdapter.Contexts(
                    activityPart = viewModel.activityPart,
                    searchPart = viewModel.searchPart,
                    popularCocktailListPart = viewModel.randomCocktailListPart,
                    filterListPart = viewModel.filterListPart,
                    cocktailListPart = viewModel.cocktailsListPart,
                )
            )
        }
    }
}

private class OverviewAdapter(
    private val lifecycleOwner: LifecycleOwner,
    private val contexts: Contexts,
) : RecyclerView.Adapter<OverviewViewHolder>() {
    private val cocktails: LiveData<List<String>>
        get() = contexts.cocktailListPart.cocktails

    val differ = AsyncListDiffer(
        ShiftedListUpdateCallback(this, FixedSize),
        AsyncDifferConfig.Builder(Differ).build()
    )

    private val list get() = differ.currentList

    init {
        cocktails.observe(lifecycleOwner) { differ.submitList(it) }
    }

    override fun getItemViewType(position: Int): Int = when (position) {
        0 -> R.layout.fragment_search_cocktail
        1 -> R.layout.fragment_popular_cocktails
        2 -> R.layout.fragment_cocktail_quick_filters
        else -> R.layout.fragment_cocktail_card
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        R.layout.fragment_search_cocktail ->
            OverviewViewHolder.SearchCocktail(lifecycleOwner, parent)
        R.layout.fragment_popular_cocktails ->
            OverviewViewHolder.PopularCocktails(lifecycleOwner, parent)
        R.layout.fragment_cocktail_quick_filters ->
            OverviewViewHolder.CocktailQuickFilters(lifecycleOwner, parent)
        else ->
            OverviewViewHolder.CocktailCard(lifecycleOwner, parent)
    }

    override fun onBindViewHolder(holder: OverviewViewHolder, position: Int) = when (holder) {
        is OverviewViewHolder.SearchCocktail -> holder.run {
            binding.searchPart = contexts.searchPart
        }
        is OverviewViewHolder.PopularCocktails -> holder.run {
            binding.activityPart = contexts.activityPart
            binding.cocktailListPart = contexts.popularCocktailListPart
        }
        is OverviewViewHolder.CocktailQuickFilters -> holder.run {
            binding.filterListPart = contexts.filterListPart
        }
        is OverviewViewHolder.CocktailCard -> holder.run {
            binding.activityPart = contexts.activityPart
            binding.cocktailPart = contexts.cocktailListPart.getCocktail(list[position - FixedSize])
            binding.cocktailListPart = contexts.cocktailListPart
        }
    }

    override fun getItemCount(): Int =
        differ.currentList.size + FixedSize

    override fun onViewAttachedToWindow(holder: OverviewViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.lifecycleOwner.onStartedResumed()
    }

    override fun onViewDetachedFromWindow(holder: OverviewViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.lifecycleOwner.onPaused()
    }

    companion object {
        private const val FixedSize = 3
    }

    private object Differ : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean =
            oldItem == newItem
    }

    private class ShiftedListUpdateCallback(
        private val adapter: RecyclerView.Adapter<*>,
        private val count: Int
    ) : ListUpdateCallback {
        override fun onInserted(position: Int, count: Int) =
            adapter.notifyItemRangeInserted(position + this.count, count)

        override fun onRemoved(position: Int, count: Int) =
            adapter.notifyItemRangeRemoved(position + this.count, count)

        override fun onMoved(fromPosition: Int, toPosition: Int) =
            adapter.notifyItemMoved(fromPosition + this.count, toPosition + this.count)

        override fun onChanged(position: Int, count: Int, payload: Any?) =
            adapter.notifyItemRangeChanged(position + this.count, count, payload)
    }

    data class Contexts(
        val activityPart: ActivityPart,
        val searchPart: SearchPart,
        val popularCocktailListPart: CocktailListPart,
        val filterListPart: FilterListPart,
        val cocktailListPart: CocktailListPart
    )
}

private sealed class OverviewViewHolder(
    binding: ViewDataBinding,
) : RecyclerView.ViewHolder(binding.root) {

    val lifecycleOwner = ControllableLifecycle()

    init {
        binding.lifecycleOwner = lifecycleOwner
        lifecycleOwner.onCreated()
    }

    class PopularCocktails(
        val binding: FragmentPopularCocktailsBinding
    ) : OverviewViewHolder(binding)

    class CocktailQuickFilters(
        val binding: FragmentCocktailQuickFiltersBinding
    ) : OverviewViewHolder(binding)

    class CocktailCard(
        val binding: FragmentCocktailCardBinding
    ) : OverviewViewHolder(binding)

    class SearchCocktail(
        val binding: FragmentSearchCocktailBinding
    ) : OverviewViewHolder(binding)

    companion object {

        fun PopularCocktails(owner: LifecycleOwner, parent: ViewGroup) = PopularCocktails(
            createBinding(parent, FragmentPopularCocktailsBinding::inflate)
        ).apply { owner.propagateDestroy(lifecycleOwner) }

        fun CocktailQuickFilters(owner: LifecycleOwner, parent: ViewGroup) = CocktailQuickFilters(
            createBinding(parent, FragmentCocktailQuickFiltersBinding::inflate)
        ).apply { owner.propagateDestroy(lifecycleOwner) }

        fun CocktailCard(owner: LifecycleOwner, parent: ViewGroup) = CocktailCard(
            createBinding(parent, FragmentCocktailCardBinding::inflate)
        ).apply { owner.propagateDestroy(lifecycleOwner) }

        fun SearchCocktail(owner: LifecycleOwner, parent: ViewGroup) = SearchCocktail(
            createBinding(parent, FragmentSearchCocktailBinding::inflate)
        ).apply { owner.propagateDestroy(lifecycleOwner) }

        private fun <T : ViewBinding> createBinding(
            parent: ViewGroup,
            bindingInflate: (LayoutInflater, ViewGroup, Boolean) -> T
        ) = bindingInflate(LayoutInflater.from(parent.context), parent, false)

        private fun LifecycleOwner.propagateDestroy(other: ControllableLifecycle) {
            lifecycle.observeOnce(ON_DESTROY) { other.onDestroy() }
        }
    }
}

