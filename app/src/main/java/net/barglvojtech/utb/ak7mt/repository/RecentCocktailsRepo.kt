package net.barglvojtech.utb.ak7mt.repository

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import net.barglvojtech.utb.ak7mt.domain.ID
import net.barglvojtech.utb.ak7mt.persistance.RecentCocktailsPreferences
import net.barglvojtech.utb.ak7mt.util.android.buildLiveData
import net.barglvojtech.utb.ak7mt.util.concurrent.Concurrency

class RecentCocktailsRepo(
    private val recentCocktailsPreferences: RecentCocktailsPreferences
) {
    private val syncChannel = Channel<List<ID>>()

    val data = buildLiveData {
        emit(recentCocktailsPreferences.load())

        Concurrency.Main.launch {
            while(true) {
                emit(syncChannel.receive())
            }
        }
    }

    suspend fun add(id: String) = coroutineScope {
        val newList = buildList {
            add(id)
            addAll(data.value?.filter { it != id } ?: emptyList())
        }

        syncChannel.send(newList)
        recentCocktailsPreferences.update(newList)
    }
}