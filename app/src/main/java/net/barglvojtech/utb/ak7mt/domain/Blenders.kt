package net.barglvojtech.utb.ak7mt.domain

fun Cocktail.blendWith(o: Cocktail?) = if (o == null) this else Cocktail(
    id = id.takeIf { id == o.id } ?: error("blending of $id and ${o.id} is not allowed"),
    name = o.name,
    category = o.category ?: category,
    imageUrl = imageUrl,
    image = o.image.takeIf { it.isNotEmpty() } ?: image,
    instructions = o.instructions ?: instructions,
    ingredients = o.ingredients.takeIf { it.isNotEmpty() } ?: ingredients
)
