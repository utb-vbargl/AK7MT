package net.barglvojtech.utb.ak7mt.domain

import net.barglvojtech.utb.ak7mt.network.Category as NetworkCategory
import net.barglvojtech.utb.ak7mt.network.Drink as NetworkCocktail
import net.barglvojtech.utb.ak7mt.network.PartialDrink as NetworkPartialCocktail
import net.barglvojtech.utb.ak7mt.persistance.Category as PersistentCategory
import net.barglvojtech.utb.ak7mt.persistance.Cocktail as PersistanceCocktail
import net.barglvojtech.utb.ak7mt.persistance.Ingredient as PersistanceIngredient
import net.barglvojtech.utb.ak7mt.persistance.Ingredient as PersistenceIngredient
import android.graphics.Bitmap
import java.security.MessageDigest
import net.barglvojtech.utb.ak7mt.persistance.CocktailWithIngredients


internal fun NetworkCocktail.toCocktail() = Cocktail(
    id = idDrink,
    name = strDrink,
    category = strCategory,
    imageUrl = strDrinkThumb,
    image = ByteArray(0),
    instructions = strInstructions,
    ingredients = listOfNotNull(
        strIngredient1 with strMeasure1,
        strIngredient2 with strMeasure2,
        strIngredient3 with strMeasure3,
        strIngredient4 with strMeasure4,
        strIngredient5 with strMeasure5,
        strIngredient6 with strMeasure6,
        strIngredient7 with strMeasure7,
        strIngredient8 with strMeasure8,
        strIngredient9 with strMeasure9,
        strIngredient10 with strMeasure10,
        strIngredient11 with strMeasure11,
        strIngredient12 with strMeasure12,
        strIngredient13 with strMeasure13,
        strIngredient14 with strMeasure14,
        strIngredient15 with strMeasure15,
    )
)

internal fun NetworkPartialCocktail.toCocktail() = Cocktail(
    id = idDrink,
    name = strDrink,
    category = null,
    imageUrl = strDrinkThumb,
    image = ByteArray(0),
    instructions = null,
    ingredients = emptyList()
)

internal fun Cocktail.toPersistentCocktail() = CocktailWithIngredients(
    cocktail = PersistanceCocktail(
        id = id,
        name = name,
        category = category,
        imageUrl = imageUrl,
        instructions = instructions
    ),
    ingredients = ingredients.map { it.toPersistentIngredient(id) }
)

internal fun Ingredient.toPersistentIngredient(cocktailId: ID) = PersistanceIngredient (
    id = id,
    cocktailId = cocktailId,
    name = name,
    volume = volume,
    unit = unit
)

internal fun CocktailWithIngredients.toCocktail() = Cocktail(
    id = cocktail.id,
    name = cocktail.name,
    category = cocktail.category,
    imageUrl = cocktail.imageUrl,
    image = ByteArray(0),
    instructions = cocktail.instructions,
    ingredients = ingredients.map { it.toIngredient() }
)

private fun PersistanceIngredient.toIngredient() = Ingredient(
    id = id,
    name = name,
    volume = volume,
    unit = unit
)


internal fun NetworkCategory.toCategory() = strCategory

internal fun PersistentCategory.toCategory() = name

internal fun Category.toPersistentCategory() = PersistentCategory(this)

// ==============
// Helpers
// -------

internal infix fun String?.with(other: String?) = when {
    this.isNullOrBlank() || other.isNullOrBlank() -> null
    else -> IngredientParser.toIngredient(this, other)
}

private object IngredientParser {
    const val ALGORITHM = "MD5"
    fun toIngredient(name: String, measureWithUnit: String): Ingredient {
        val splitted = measureWithUnit.split(" ").filterNot(String::isBlank)
        val id = run {
            MessageDigest.getInstance(ALGORITHM).apply {
                update(name.toByteArray())
                update(measureWithUnit.toByteArray())
            }.digest().toString(Charsets.UTF_8)
        }

        return Ingredient(
            id = id,
            name = name,
            volume = splitted.dropLast(1).joinToString(separator = " "),
            unit = splitted.last()
        )
    }
}