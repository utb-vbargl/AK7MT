package net.barglvojtech.utb.ak7mt.viewmodel.part

import androidx.lifecycle.LiveData
import net.barglvojtech.utb.ak7mt.domain.Cocktail
import net.barglvojtech.utb.ak7mt.domain.Ingredient
import net.barglvojtech.utb.ak7mt.util.android.buildLiveData
import net.barglvojtech.utb.ak7mt.util.android.deriveLiveData

interface CocktailPart {
    val cocktail: LiveData<Cocktail>

    val name: LiveData<String>
    val category: LiveData<String?>
    val imageUrl: LiveData<String>
    val image: LiveData<ByteArray>
    val instructions: LiveData<String?>

    val ingredients: LiveData<List<Ingredient>>
    val ingredientSize: LiveData<Int>
    fun getIngredient(itemPosition: Int): IngredientPart
}

open class SimpleCocktailPart(
    override val cocktail: LiveData<Cocktail>,
) : CocktailPart {

    override val name by lazy { deriveLiveData(cocktail) { it.name } }
    override val category by lazy { deriveLiveData(cocktail) { it.category } }
    override val imageUrl by lazy { deriveLiveData(cocktail) { it.imageUrl } }
    override val image by lazy { deriveLiveData(cocktail) { it.image } }
    override val instructions by lazy { deriveLiveData(cocktail) { it.instructions } }

    override val ingredients by lazy { deriveLiveData(cocktail) { it.ingredients } }
    override val ingredientSize by lazy { deriveLiveData(ingredients) { it.size } }

    override fun getIngredient(itemPosition: Int): IngredientPart {
        val data = buildLiveData {
            observe(ingredients) {
                if (itemPosition < it.size) {
                    emit(it[itemPosition])
                }
            }
        }

        return SimpleIngredientPart(data)
    }
}


