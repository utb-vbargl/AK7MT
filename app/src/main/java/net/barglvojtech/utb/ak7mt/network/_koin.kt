package net.barglvojtech.utb.ak7mt.network

import io.ktor.client.engine.HttpClientEngineFactory
import io.ktor.client.engine.cio.CIO
import org.koin.core.module.dsl.new
import org.koin.dsl.module

object NetworkKoin {
    val module = module {
        single<HttpClientEngineFactory<*>> { CIO }
        single { new(::SimpleHttpService) }
        single { CocktailDbService(CocktailDbService.Configuration(), get()) }
    }
}