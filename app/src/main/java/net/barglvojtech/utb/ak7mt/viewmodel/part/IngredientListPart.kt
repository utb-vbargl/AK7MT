package net.barglvojtech.utb.ak7mt.viewmodel.part

import androidx.lifecycle.LiveData
import net.barglvojtech.utb.ak7mt.domain.Ingredient

interface IngredientListPart {
    val ingredients: LiveData<List<Ingredient>>
    fun get()
}