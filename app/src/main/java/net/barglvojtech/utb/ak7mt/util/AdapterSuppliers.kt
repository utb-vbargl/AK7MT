package net.barglvojtech.utb.ak7mt.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import net.barglvojtech.utb.ak7mt.databinding.FragmentFilterChipBinding
import net.barglvojtech.utb.ak7mt.databinding.FragmentIngredientCheckboxBinding
import net.barglvojtech.utb.ak7mt.databinding.FragmentPopularCocktailCardBinding
import net.barglvojtech.utb.ak7mt.domain.Ingredient
import net.barglvojtech.utb.ak7mt.viewmodel.part.ActivityPart
import net.barglvojtech.utb.ak7mt.viewmodel.part.CocktailListPart
import net.barglvojtech.utb.ak7mt.viewmodel.part.CocktailPart
import net.barglvojtech.utb.ak7mt.viewmodel.part.FilterListPart
import net.barglvojtech.utb.ak7mt.util.android.ControllableLifecycle
import net.barglvojtech.utb.ak7mt.util.android.observe
import net.barglvojtech.utb.ak7mt.util.android.buildLiveData
import net.barglvojtech.utb.ak7mt.util.android.observeOnce
import net.barglvojtech.utb.ak7mt.viewmodel.Filter

object AdapterSuppliers {
    @JvmStatic fun popularCocktails(
        activityPart: ActivityPart,
        cocktailListPart: CocktailListPart,
    ) = AdapterSupplier.LiveDataAdapterSupplier {
        val contexts = PopularCocktailsAdapter.Contexts(activityPart, cocktailListPart)
        PopularCocktailsAdapter(it, contexts)
    }

    private class PopularCocktailsAdapter(
        lifecyclyOwner: LifecycleOwner,
        private val contexts: Contexts
    ) : BindingAdapter<String, FragmentPopularCocktailCardBinding>(
        lifecyclyOwner, contexts.cocktailListPart.cocktails, comparator()
    ) {

        override fun createBinding(parent: ViewGroup, viewType: Int) =
            FragmentPopularCocktailCardBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )

        override fun onBindViewHolder(
            holder: BindingViewHolder<FragmentPopularCocktailCardBinding>,
            position: Int
        ) = bind(holder, position) { binding, item ->
            binding.activityPart = contexts.activityPart
            binding.cocktailListPart = contexts.cocktailListPart
            binding.cocktailPart = when {
                position < itemCount -> contexts.cocktailListPart.getCocktail(item)
                else -> null
            }
        }

        data class Contexts(
            val activityPart: ActivityPart,
            val cocktailListPart: CocktailListPart,
        )
    }

    @JvmStatic fun filters(filterListPart: FilterListPart): AdapterSupplier =
        AdapterSupplier.LiveDataAdapterSupplier { FiltersAdapter(it, filterListPart) }

    private class FiltersAdapter(
        lifecycleOwner: LifecycleOwner,
        private val filterListPart: FilterListPart,
    ) : BindingAdapter<Filter, FragmentFilterChipBinding>(
        lifecycleOwner, filterListPart.filters, comparator()
    ) {
        override fun createBinding(parent: ViewGroup, viewType: Int) =
            FragmentFilterChipBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )

        override fun onBindViewHolder(
            holder: BindingViewHolder<FragmentFilterChipBinding>,
            position: Int
        ) = bind(holder, position) { binding, filter ->
            binding.filter = filter
            binding.filterListPart = filterListPart
        }
    }

    @JvmStatic fun ingredients(cocktailPart: CocktailPart): AdapterSupplier =
        AdapterSupplier.LiveDataAdapterSupplier { IngredientAdapter(it, cocktailPart) }

    private class IngredientAdapter(
        lifecyclyOwner: LifecycleOwner,
        private val cocktailPart: CocktailPart
    ) : BindingAdapter<Ingredient, FragmentIngredientCheckboxBinding>(
        lifecyclyOwner, cocktailPart.ingredients, comparator()
    ) {
        override fun createBinding(parent: ViewGroup, viewType: Int ) =
            FragmentIngredientCheckboxBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )

        override fun onBindViewHolder(
            holder: BindingViewHolder<FragmentIngredientCheckboxBinding>,
            position: Int
        ) = bind(holder, position) { binding, _ ->
            binding.ingredientPart = cocktailPart.getIngredient(position)
        }
    }

    abstract class BindingAdapter<T : Any, B : ViewDataBinding>(
        private val lifecycleOwner: LifecycleOwner,
        liveData: LiveData<List<T>>,
        comparator: Comparator<T>
    ) : ListAdapter<T, BindingAdapter.BindingViewHolder<B>>(ComparatorDiff(comparator)) {

        protected val data = buildLiveData { observe(liveData) }
            .apply { observe(lifecycleOwner) { submitList(it) } }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<B> {
            val binding = createBinding(parent, viewType)
            val viewHolder = BindingViewHolder(binding)
            binding.lifecycleOwner = viewHolder
            viewHolder.lifecycleOwner.onCreated()
            lifecycleOwner.lifecycle.observeOnce(Lifecycle.Event.ON_DESTROY) { viewHolder.lifecycleOwner.onDestroy() }
            return viewHolder
        }

        protected abstract fun createBinding(parent: ViewGroup, viewType: Int): B

        override fun onViewAttachedToWindow(holder: BindingViewHolder<B>) {
            super.onViewAttachedToWindow(holder)
            holder.lifecycleOwner.onStartedResumed()
        }

        override fun onViewDetachedFromWindow(holder: BindingViewHolder<B>) {
            super.onViewDetachedFromWindow(holder)
            holder.lifecycleOwner.onPaused()
        }

        companion object {
            fun <T : Any, B : ViewDataBinding> BindingAdapter<T, B>.bind(
                holder: BindingViewHolder<B>,
                position: Int,
                onBinding: (B, T) -> Unit
            ) {
                val item = data.value?.elementAtOrNull(position) ?: return
                onBinding(holder.binding, item)
                holder.binding.executePendingBindings()
            }

            inline fun <reified T> comparator(): Comparator<T> =
                java.util.Comparator<T> { o1, o2 -> if (o1 == o2) 0 else 1 }
        }

        class ComparatorDiff<T : Any>(
            private val comparator: Comparator<T>
        ) : DiffUtil.ItemCallback<T>() {
            override fun areItemsTheSame(oldItem: T, newItem: T): Boolean =
                comparator.compare(oldItem, newItem) == 0

            override fun areContentsTheSame(oldItem: T, newItem: T): Boolean =
                comparator.compare(oldItem, newItem) == 0
        }

        class BindingViewHolder<T : ViewDataBinding>(
            val binding: T,
            val lifecycleOwner: ControllableLifecycle = ControllableLifecycle()
        ) : RecyclerView.ViewHolder(binding.root), LifecycleOwner by lifecycleOwner
    }
}