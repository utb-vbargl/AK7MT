# Mobilní aplikace závěrečného projektu by měla plnit rozsahově následující kritéria:

* zdrojové kódy přístupné v nějakém verzovacím systému (GIT)
* vlastní ikona aplikace, vlastní splashscreen
* více propojených obrazovek, funkční navigační stack (přechody mezi obrazovkami)
* ukládání dat do perzistentní paměti zařízení
* vstup od uživatele (zadávání dat uživatelem)
* komunikace s libovolným REST API
* v krajním případě (odůvodněno například praxí - tím, že danou technologii, jazyk, FW používáte v práci) lze zvolit libovolnou platformu (Android, iOS), programovací jazyk či vývojový framework - jinak je preferováno řešení v Kotlinu pro Android s využitím doporučených návrhových vzorů

Není nutné připravovat oficiální prezentaci, jde o předvedení funkčnosti a stručný popis toho, co aplikace řeší a jak.
